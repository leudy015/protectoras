import React from "react";
import "./App.scss";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import PivateRouter from "./PrivateRouter";
import PublicRouter from "./PublicRouter";

// Components
import Session from "./Sesion";
//Screen

import Login from "./Pages/Login";
import Profile from "./Pages/Profile";
import Forgot from "./Container/RecoverPassword";
import Reset from "./Container/RecoverPassword/Reset";
import Register from "./Container/ResgisterClient";
import Home from "./Pages/Home";
import NoFound from "./Pages/Nofound";

class App extends React.Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Switch>
              <Route exact path="/" component={Home} />
              <PublicRouter exact path="/login" component={Login} />
              <PivateRouter exact path="/admin" component={Profile} />
              <PublicRouter
                exact
                path="/oauth/v2/recover-password"
                component={Forgot}
              />
              <PublicRouter
                exact
                path="/oauth/v2/reset-password/:token"
                component={Reset}
              />
              <PublicRouter exact path="/register" component={Register} />
              <Route component={NoFound} />
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}
const RootSession = Session(App);

export { RootSession };
