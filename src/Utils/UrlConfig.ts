export const LOCAL_API_URL = "https://server.vetec.es";
export const LOCAL_API_PATH = "/graphql";
export const IMAGES_PATH = `${LOCAL_API_URL}/assets/images/`;
export const FACEBOOK_APP_ID = "1035758230197539";
export const INSTAGRAM_APP_ID = "612522072710186";
export const GOOGLE_CLIENT_ID =
  "276965335350-247knqjbaetgnknqc4t4n4etmeos3bc2.apps.googleusercontent.com";
