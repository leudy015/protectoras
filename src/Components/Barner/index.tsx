import React from "react";
import "./index.scss";
import { Headers } from "../Header";

export default function Barners(props: any) {
  const { title, subtitle, btntitle, href, btn } = props;
  return (
    <>
      <Headers />
      <div className="containers_barnes">
        <div className="containers_barnes_children">
          <div>
            <h1>{title}</h1>
            <p>{subtitle}</p>
            {btn ? (
              <a className="btn-help" href={href}>
                {btntitle}
              </a>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}
