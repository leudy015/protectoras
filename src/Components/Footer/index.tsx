import React, { useState } from "react";
import "./index.scss";
import {
  TwitterOutlined,
  InstagramFilled,
  FacebookFilled,
  LinkedinFilled,
  TransactionOutlined,
  MailOutlined,
  RightOutlined,
  PhoneOutlined,
  EnvironmentOutlined,
} from "@ant-design/icons";
import { Select, message, Modal } from "antd";

const MainLogo = require("../../Assets/images/icon.png");

const { Option } = Select;

const Footer = () => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <div className="Footer">
        <div className="Footer-Content">
          <div className="SuBitems">
            <img src={MainLogo} style={{ width: 60, marginBottom: 15 }} />
            <div>
              <p className="text">
                ¡Los mejores veterinarios en la palma de tu mano! Encuentra
                profesionales de confianza y realiza tus consultas de forma
                fácil y segura.
              </p>
            </div>
            <div style={{ display: "flex", marginTop: 20 }}>
              <a
                href="https://twitter.com/vetecapp"
                className="icons"
                target="_blank"
              >
                <TwitterOutlined style={{ fontSize: 28, marginRight: 15 }} />
              </a>
              <a
                href="https://www.facebook.com/vetecapp"
                className="icons"
                target="_blank"
              >
                <FacebookFilled style={{ fontSize: 28, marginRight: 15 }} />
              </a>
              <a
                href="https://www.instagram.com/vetecapp/"
                className="icons"
                target="_blank"
              >
                <InstagramFilled style={{ fontSize: 28, marginRight: 15 }} />
              </a>
              <a
                href="https://www.linkedin.com/company/65300330/admin/"
                className="icons"
                target="_blank"
              >
                <LinkedinFilled style={{ fontSize: 28, marginRight: 15 }} />
              </a>
            </div>
          </div>
          <div className="SuBitemsxs">
            <ul>
              <li>
                <span className="SuBitemsspan">EMPRESA</span>
              </li>
              <li>
                <a
                  href="https://vetec.es/ventajas"
                  style={{ cursor: "pointer" }}
                >
                  ¿Cómo funciona?
                </a>
              </li>
              <li>
                <a href="https://vetec.es/quienes-somos">¿Qué es Vetec?</a>
              </li>
              <li>
                <a href="https://vetec.es/team">Nuestro equipo</a>
              </li>
              <li>
                <a href="https://blog.vetec.es" target="_blank">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://blog.vetec.es">Prensa</a>
              </li>
              <li>
                <a
                  onClick={() =>
                    message.warning("Aún no tenemos vacantes disponible")
                  }
                >
                  Trabaja con nosotros
                </a>
              </li>
            </ul>
          </div>
          <div className="SuBitemsxs">
            <ul>
              <li>
                <span className="SuBitemsspan">ASUNTOS LEGALES</span>
              </li>
              <li>
                <a href="https://vetec.es/contacto">Contacto</a>
              </li>
              <li>
                <a href="https://vetec.es/preguntas-frecuentes">
                  Preguntas frecuentes
                </a>
              </li>
              <li>
                <a href="https://vetec.es/privacidad">Privacidad</a>
              </li>
              <li>
                <a href="https://vetec.es/condiciones">Condiciones de uso</a>
              </li>
              <li>
                <a href="https://vetec.es/legal">Aviso Legal</a>
              </li>
              <li>
                <a href="https://vetec.es/cookies">Cookies</a>
              </li>
            </ul>
          </div>
          <div className="SuBitems">
            <Select defaultValue="españa" style={{ width: 140 }}>
              <Option value="españa">
                {" "}
                <span>🇪🇸</span> España
              </Option>
            </Select>
            <Select
              defaultValue="English"
              style={{ width: 140, marginTop: 20, color: "black" }}
              bordered={false}
            >
              <Option value="language">
                {" "}
                <TransactionOutlined
                  style={{ color: "#647DEE", marginRight: 5 }}
                />{" "}
                Language
              </Option>
              <Option value="English">
                <span>🇪🇸</span> Español
              </Option>
            </Select>
          </div>
        </div>
        <div className="Subfooter" />
        <div className="certificados">
          <p style={{ color: "gray", marginRight: 20 }}>© 2020 Vetec App</p>
        </div>
      </div>
      <Modal
        title="Contact"
        visible={visible}
        footer={false}
        onCancel={() => setVisible(false)}
      >
        <div style={{ margin: 20, paddingBottom: 30 }}>
          <div className="item_contact">
            <MailOutlined
              style={{ marginRight: 15, color: "#647DEE", fontSize: 22 }}
            />{" "}
            <a
              href="mailto://info@islink.es"
              style={{ color: "black" }}
              target="_blank"
            >
              info@islink.es
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <PhoneOutlined
              style={{ marginRight: 15, color: "#647DEE", fontSize: 22 }}
            />{" "}
            <a href="tel:+947202419" style={{ color: "black" }}>
              Contact support
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
          <div className="item_contact">
            <EnvironmentOutlined
              style={{ marginRight: 15, color: "#647DEE", fontSize: 22 }}
            />{" "}
            <a href="/" style={{ color: "black" }}>
              Spain ES.
            </a>
            <RightOutlined style={{ marginLeft: "auto" }} />
          </div>
        </div>
      </Modal>
    </>
  );
};

export default Footer;
