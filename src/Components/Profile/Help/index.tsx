import React from "react";
import { Button, Alert } from "antd";
import "./hepl.scss";

export default function Help() {
  return (
    <div className="accounthelp">
      <h1>Ayuda</h1>

      <Alert
        style={{ textAlign: "left", maxWidth: 500 }}
        message="Proponer cambios"
        description="Estamos aquí para satisfacer sus necesidades, por lo que estamos abiertos a agregar funcionalidades recomendadas por nuestros miembros."
        type="success"
        showIcon
        closable
      />
      <div className="conthelp">
        <h2>Necesitas ayuda</h2>
        <div className="conthelp_account_set">
          <p>Enviar correo electrónico</p>
          <Button
            href="mailto:info@vetec.es"
            type="primary"
            shape="round"
            style={{ width: 280 }}
          >
            Enviar correo electrónico al soporte
          </Button>
        </div>
      </div>

      <div className="conthelp">
        <h2>Proponer un cambio</h2>
        <div className="conthelp_account_set">
          <p>Enviar correo electrónico para proponer un cambio</p>
          <Button href="mailto:info@vetev.es" type="primary" shape="round">
            Enviar correo electrónico
          </Button>
        </div>
      </div>
    </div>
  );
}
