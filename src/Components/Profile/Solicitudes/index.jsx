import React, { useState } from "react";
import "./solicitud.scss";
import { useQuery, useMutation } from "react-apollo";
import { GET_SOLICITUD } from "../../../GraphQL/query";
import {
  ACTUALIZAR_SOLICITUD,
  ACTUALIZAR_MASCOTA,
} from "../../../GraphQL/mutation";
import { IMAGES_PATH, LOCAL_API_URL } from "../../../Utils/UrlConfig";
import { Tag, Tooltip, Modal, Button, message, Spin } from "antd";
import {
  CheckCircleOutlined,
  SyncOutlined,
  CloseCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import moment from "moment";
import Nodata from "../../../Assets/images/nodata.png";

export default function Solicitudes(props) {
  const { user } = props;
  const [estado, setestado] = useState("Solicitado");
  const { data, refetch, loading } = useQuery(GET_SOLICITUD, {
    variables: { protectora: user.id, estado: estado },
  });

  const [visible, setVisible] = useState(false);
  const [dats, setdats] = useState(null);
  const [actualizarsolicitud] = useMutation(ACTUALIZAR_SOLICITUD);
  const [actualizarMascota] = useMutation(ACTUALIZAR_MASCOTA);

  const solicitud =
    data && data.getAdopcionProtectora ? data.getAdopcionProtectora.data : [];

  const rechazarSolicitud = (dats) => {
    const { confirm } = Modal;
    confirm({
      title: "Esta segur@ que deseas rechazar la solicitud?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Lamentanos que el solicitante no se pueda quedar con la mascota",
      okText: "Rechazar Solicitud",
      onOk() {
        procesarSolicitud(dats, false);
      },
      onCancel() {},
    });
  };

  const SendPushNotificationNeworden = (UserID, messa) => {
    fetch(
      `${LOCAL_API_URL}/send-push-notification?IdOnesignal=${UserID}&textmessage=${messa}`
    ).catch((err) => console.log(err));
  };

  const SendTextSMSNeworden = (telefono, messa) => {
    fetch(
      `${LOCAL_API_URL}/send-message?recipient=${telefono}&textmessage=${messa}`
    ).catch((err) => console.log(err));
  };

  const procesarSolicitud = (datos, aceptar) => {
    const input = {
      id: datos.id,
      name: datos.name,
      lastName: datos.lastName,
      email: datos.email,
      city: datos.city,
      phone: datos.phone,
      experiencia: datos.experiencia,
      otra: datos.otra,
      fecha: datos.fecha,
      nino: datos.nino,
      mascota: datos.mascota,
      usuario: datos.usuario,
      protectora: datos.protectora,
      estado: aceptar ? "Aprobado" : "Rechazado",
      razon: aceptar ? null : "Tu solicitud ha sido rechada",
    };

    actualizarsolicitud({ variables: { input: input } }).then((res) => {
      if (res.data.actualizarsolicitud.success) {
        message.success("Solicitud procesada con éxito");
        setVisible(false);
        refetch();
        if (aceptar) {
          SendPushNotificationNeworden(
            datos.UserID,
            `Tu solicitud de adopción ha sido aceptada ponte en contacto con la protectora, esperamos que disfute de ${datos.macotas.name}`
          );
          SendTextSMSNeworden(
            datos.phone,
            `Tu solicitud de adopción ha sido aceptada ponte en contacto con la protectora, esperamos que disfute de ${datos.macotas.name}`
          );
        } else {
          SendPushNotificationNeworden(
            datos.UserID,
            `Tu solicitud de adopción ha sido rechazado lamentanos los inconveniente`
          );
          SendTextSMSNeworden(
            datos.phone,
            `Tu solicitud de adopción ha sido rechazado lamentanos los inconveniente`
          );
        }
        if (aceptar) {
          actualizarMascota({
            variables: {
              input: {
                id: datos.mascota,
                usuario: datos.usuario,
                protectoraID: null,
                protectora: false,
              },
            },
          });
        }
      }
    });
  };

  return (
    <>
      <div className="account">
        <h1>Solicitudes de adopción</h1>
        <div className="cont">
          <h2>Solicitudes</h2>
          <div className="cont_account_set">
            <h2>Filtros</h2>
            <div style={{ marginBottom: 30, display: "flex" }}>
              <Button
                type="dashed"
                style={{ marginLeft: 15 }}
                onClick={() => setestado("Solicitado")}
              >
                Solicitado
              </Button>
              <Button
                type="dashed"
                style={{ marginLeft: 15 }}
                onClick={() => setestado("Aprobado")}
              >
                Aprobado
              </Button>
              <Button
                type="dashed"
                danger
                style={{ marginLeft: 15 }}
                onClick={() => setestado("Rechazado")}
              >
                Rechazado
              </Button>
            </div>
            {solicitud.length > 0 ? (
              <div>
                {loading ? (
                  <div
                    style={{
                      textAlign: "center",
                      justifyContent: "center",
                      alignItems: "center",
                      display: "flex",
                      height: "60vh",
                    }}
                  >
                    <Spin size="large" />
                  </div>
                ) : (
                  <>
                    {solicitud &&
                      solicitud.map((da, i) => {
                        return (
                          <Tooltip title="Click para ver detalles" key={i}>
                            <div
                              className="card__solicitud"
                              onClick={() => {
                                setVisible(true);
                                setdats(da);
                              }}
                            >
                              <div className="card__solicitud_im">
                                <img
                                  src={IMAGES_PATH + da.macotas.avatar}
                                  alt=""
                                />
                              </div>

                              <div className="card__solicitud_info">
                                <h2>{da.macotas.name}</h2>
                                <p>
                                  {da.name} {da.lastName}
                                </p>
                                <p style={{ paddingTop: 7 }}>{da.city}</p>
                              </div>

                              <div className="card__solicitud_extra">
                                <h3>Fecha</h3>
                                <p>{moment(da.created_at).format("ll")}</p>
                                <h3>Estado</h3>
                                {da.estado === "Solicitado" ? (
                                  <Tag
                                    color="processing"
                                    icon={<SyncOutlined spin />}
                                  >
                                    {da.estado}
                                  </Tag>
                                ) : null}

                                {da.estado === "Aprobado" ? (
                                  <Tag
                                    icon={<CheckCircleOutlined />}
                                    color="success"
                                  >
                                    {da.estado}
                                  </Tag>
                                ) : null}

                                {da.estado === "Rechazado" ? (
                                  <Tag
                                    icon={<CloseCircleOutlined />}
                                    color="error"
                                  >
                                    {da.estado}
                                  </Tag>
                                ) : null}
                              </div>
                            </div>
                          </Tooltip>
                        );
                      })}
                  </>
                )}
              </div>
            ) : (
              <div className="no_data">
                <img src={Nodata} alt="" />
                <h3>Aún no haz recibido solicitudes</h3>
              </div>
            )}
          </div>
        </div>
      </div>
      {dats ? (
        <Modal
          title="Detalles de la solicitud"
          visible={visible}
          footer={null}
          onCancel={() => {
            setVisible(false);
            setdats(null);
          }}
        >
          <div className="detalles">
            <img src={IMAGES_PATH + dats.macotas.avatar} alt="" />

            <div className="detalles__info_soli">
              <h2>Información de Solicitante</h2>

              <p>Nombre: {dats.name}</p>
              <p>Apellidos: {dats.lastName}</p>
              <p>Ciudad: {dats.city}</p>
              <p>Teléfono: {dats.phone}</p>

              <h2>Sobre su experiencia</h2>

              <p>Experiencia con mascotas: {dats.experiencia}</p>
              <p>¿Tienes otra mascota en casa?: {dats.otra}</p>
              <p>¿Tienes niños?: {dats.nino ? "Si" : "No"}</p>

              <h2>Contactar en fecha:</h2>

              <p>Contactar en fecha: {moment(dats.fecha).format("lll")}</p>
              <p>Via teléfonica</p>
            </div>

            {dats.estado === "Solicitado" ? (
              <div style={{ display: "flex" }}>
                <Button
                  onClick={() => rechazarSolicitud(dats)}
                  type="primary"
                  danger
                  shape="round"
                  style={{ height: 50, width: 250, margin: 5 }}
                >
                  Rechazar Solicitud
                </Button>

                <Button
                  onClick={() => procesarSolicitud(dats, true)}
                  type="primary"
                  shape="round"
                  style={{ height: 50, width: 250, margin: 5 }}
                >
                  Aprobar solicitud
                </Button>
              </div>
            ) : null}
          </div>
        </Modal>
      ) : null}
    </>
  );
}
