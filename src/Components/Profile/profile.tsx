import React, { useState } from "react";
import "./profile.scss";
import { Button, Modal } from "antd";
import {
  PlusCircleOutlined,
  UserOutlined,
  AlignLeftOutlined,
  CloseOutlined,
  QuestionCircleOutlined,
  LogoutOutlined,
  ExclamationCircleOutlined,
  DiffOutlined,
} from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import Account from "./Account";
import Mascota from "./Mascotas";
import Help from "./Help";
import Logo from "../../Assets/images/profile.svg";
import Solicitudes from "./Solicitudes";

function Profile(props: any) {
  const { history, data, refetch } = props;
  const [menuShown, setMenuShown] = useState(false);
  const [active_index, setActive_index] = useState(1);

  const changeTab = (index: number) => {
    setActive_index(index);
  };

  const toggleMenuMobile = () => {
    setMenuShown(!menuShown);
  };

  const cerrarSesionUsuario = () => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que deseas cerra sesión?",
      icon: <ExclamationCircleOutlined />,
      content: "Esperamos verte pronto por aquí",
      okText: "Cerrar sesión",
      cancelText: "Cancelar",
      onOk() {
        localStorage.removeItem("token");
        localStorage.removeItem("id");
        history.push("/login");
      },
      onCancel() {},
    });
  };

  return (
    <div className="content main-wrapper">
      <div className={`topnav ${menuShown ? "visible-mobile" : ""}`}>
        <div className="toggle-nav" onClick={() => toggleMenuMobile()}>
          {menuShown ? (
            <CloseOutlined style={{ fontSize: 28, color: "#647DEE" }} />
          ) : (
            <AlignLeftOutlined style={{ fontSize: 28, color: "#647DEE" }} />
          )}
        </div>
        <div className="nav-items" onClick={() => toggleMenuMobile()}>
          <div className={active_index === 0 ? "active simple" : "simple"}>
            <a href="/">
              <img src={Logo} alt="isLink" />
            </a>
          </div>
          <Button
            className={active_index === 1 ? "active simple" : "simple"}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={() => changeTab(1)}
          >
            <PlusCircleOutlined />
            <h3 className="primary">MIS MASCOTAS</h3>
          </Button>

          <Button
            className={active_index === 4 ? "active simple" : "simple"}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={() => changeTab(4)}
          >
            <DiffOutlined />
            <h3 className="primary">SOLICITUDES</h3>
          </Button>

          <Button
            className={active_index === 2 ? "active simple" : "simple"}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={() => changeTab(2)}
          >
            <UserOutlined />
            <h3 className="primary">MI CUENTA</h3>
          </Button>
          <Button
            className={active_index === 5 ? "active simple" : "simple"}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={() => changeTab(5)}
          >
            <QuestionCircleOutlined />
            <h3 className="primary">AYUDA</h3>
          </Button>

          <Button
            className={active_index === 0 ? "active simple" : "simple"}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
            onClick={() => cerrarSesionUsuario()}
          >
            <LogoutOutlined />
            <h3 className="primary">SALIR</h3>
          </Button>
        </div>
      </div>
      <div className="page-content">
        {active_index === 1 ? <Mascota user={data} /> : null}
        {active_index === 4 ? <Solicitudes user={data} /> : null}
        {active_index === 2 ? <Account user={data} refetch={refetch} /> : null}
        {active_index === 5 ? <Help /> : null}
      </div>
    </div>
  );
}

export default withRouter(Profile);
