import React, { useState, useEffect } from "react";
import "./account.scss";
import { Input, Button, Avatar, message, Upload, Modal } from "antd";
import {
  UserOutlined,
  MailOutlined,
  PhoneOutlined,
  ExclamationCircleOutlined,
  LinkOutlined,
  EnvironmentOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import { useMutation } from "react-apollo";
import {
  ACTUALIZAR_USUARIO,
  UPLOAD_FILE,
  ELIMINAR_USUARIO,
} from "../../../GraphQL/mutation";
import { withRouter } from "react-router-dom";

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

function Account(props: any) {
  const { user, history, refetch } = props;
  const [name, setName] = useState(user.name);
  const [avatar, setAvatar] = useState(user.logo);
  const [phone, setPhone] = useState(user.phone);
  const [email, setEmail] = useState(user.email);
  const [web, setWeb] = useState(user.web);
  const [descrioption, setDescrioption] = useState(user.descripcion);
  const [city, setCity] = useState(user.adrees ? user.adrees.ciudad : "");
  const [latitude, setLatitude] = useState(user.adrees ? user.adrees.lat : "");
  const [longitude, setLongitude] = useState(
    user.adrees ? user.adrees.lgn : ""
  );
  const [calle, setCalle] = useState(user.adrees ? user.adrees.calle : "");
  const [cp, setCp] = useState(user.adrees ? user.adrees.cp : "");
  const [numero, setNumero] = useState(user.adrees ? user.adrees.numero : "");
  const [provincia, setProvincia] = useState(
    user.adrees ? user.adrees.provincia : ""
  );
  const [actualizarProtectora] = useMutation(ACTUALIZAR_USUARIO);
  const [eliminarProtectora] = useMutation(ELIMINAR_USUARIO);
  const [singleUpload] = useMutation(UPLOAD_FILE);

  const updateUser = (avatar: any, messages: string) => {
    const input = {
      id: user.id,
      logo: avatar,
    };
    actualizarProtectora({ variables: { input: input } })
      .then((res) => {
        console.log(res);
        if (res.data.actualizarProtectora.success) {
          message.success(messages);
          refetch();
        } else {
          message.error("Algo anda mal inténtalo de nuevo");
        }
      })
      .catch((err) => {
        console.log(err);
        message.error("Algo anda mal inténtalo de nuevo");
      });
  };

  const uplodaAvatar = (imgBlob: any) => {
    singleUpload({ variables: { imgBlob } })
      .then((res: any) => {
        const filename =
          res && res.data && res.data.singleUpload
            ? res.data.singleUpload.filename
            : "";
        setAvatar(filename);
        updateUser(filename, "imagen de perfil agregada correctamente");
      })
      .catch((error: any) => {
        console.log("fs error: ", error);
      });
  };

  const resetPassword = () => {
    localStorage.removeItem("id");
    localStorage.removeItem("token");
    history.push("/oauth/v2/recover-password");
  };

  const deleteAccount = () => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás segura de que quieres eliminar tu cuenta?",
      icon: <ExclamationCircleOutlined />,
      content:
        "eliminar su cuenta elimina todos los datos relacionados con ella.",
      onOk() {
        eliminarProtectora({ variables: { id: user.id } }).then((res: any) => {
          if (res.data.eliminarProtectora.success) {
            localStorage.removeItem("id");
            localStorage.removeItem("token");
            history.push("/register");
          } else {
            message.error("Algo anda mal inténtalo de nuevo");
          }
        });
      },
      onCancel() {},
    });
  };

  const updateAccount = () => {
    const input = {
      id: user.id,
      name: name,
      email: email,
      logo: avatar,
      adrees: {
        calle: calle,
        cp: cp,
        numero: numero,
        ciudad: city,
        provincia: provincia,
        lat: latitude,
        lgn: longitude,
      },
      phone: phone,
      descripcion: descrioption,
      web: web,
    };
    actualizarProtectora({ variables: { input: input } })
      .then((res) => {
        if (res.data.actualizarProtectora.success) {
          message.success("Datos actualizados con éxito");
          refetch();
        } else {
          message.error("Algo anda mal inténtalo de nuevo");
        }
      })
      .catch(() => {
        message.error("Algo anda mal inténtalo de nuevo");
      });
  };

  useEffect(() => {
    function getGeoLocation() {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;

            setLatitude(latitude.toString());
            setLongitude(longitude.toString());

            let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyCXSjKVdrHS59rYoc_zISqcobAftAHw2cg`;
            fetch(apiUrlWithParams)
              .then((response) => response.json())
              .then((data) => {
                let cityFound = false;

                for (let index = 0; index < data.results.length; index++) {
                  for (
                    let i = 0;
                    i < data.results[index].address_components.length;
                    i++
                  ) {
                    if (
                      data.results[index].address_components[i].types.includes(
                        "locality"
                      )
                    ) {
                      setCity(
                        data.results[index].address_components[i].long_name
                      );

                      setCalle(
                        data.results[index].address_components[1].long_name
                      );
                      setCp(
                        data.results[index].address_components[6].long_name
                      );

                      setNumero(
                        data.results[index].address_components[0].long_name
                      );

                      setProvincia(
                        data.results[index].address_components[4].long_name
                      );
                      cityFound = true;
                    }

                    if (cityFound) break;
                  }

                  if (cityFound) break;
                }
              })
              .catch((error) => {
                console.log("error in google geocode api: ", error);
              });
          },
          (error) => console.log("geolocation error", error)
        );
      } else {
        console.log("this browser not supported HTML5 geolocation API");
      }
    }

    getGeoLocation();
  }, [city]);

  return (
    <div className="account">
      <h1>Mi cuenta</h1>
      <div className="cont">
        <h2>Mi información</h2>
        <div className="cont_account_set">
          <p>Nombre</p>
          <Input
            size="large"
            placeholder="Nombre"
            onChange={(e) => setName(e.target.value)}
            defaultValue={name}
            prefix={<UserOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />
          <p>Teléfono</p>
          <Input
            size="large"
            placeholder="Teléfono"
            onChange={(e) => setPhone(e.target.value)}
            defaultValue={phone}
            prefix={<PhoneOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />
          <p>Correo Electrónico</p>
          <Input
            size="large"
            type="email"
            defaultValue={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Correo Electrónico"
            prefix={<MailOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />

          <Input
            size="large"
            type="email"
            defaultValue={descrioption}
            onChange={(e) => setDescrioption(e.target.value)}
            placeholder="Descripción"
            prefix={<UnorderedListOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />

          <Button
            onClick={() => updateAccount()}
            type="primary"
            shape="round"
            style={{ height: 50, width: 250 }}
          >
            Guardar cambios
          </Button>
        </div>
      </div>

      <div className="cont">
        <h2>Mi fotos de perfil</h2>
        <div className="cont_account_set" style={{ display: "flex" }}>
          <div className="lef_cont">
            {!user.logo ? (
              <Avatar size={64} icon={<UserOutlined />} />
            ) : (
              <Avatar size={64} src={IMAGES_PATH + avatar} />
            )}

            <Upload
              name="avatar"
              listType="picture"
              showUploadList={false}
              customRequest={async (data: any) => {
                let imgBlob = await getBase64(data.file);
                uplodaAvatar(imgBlob);
              }}
            >
              <Button type="primary" shape="round" style={{ marginTop: 20 }}>
                Seleccionar imagen
              </Button>
            </Upload>
          </div>

          <div className="contenido">
            <p>Página web</p>
            <Input
              size="large"
              type="url"
              defaultValue={web}
              onChange={(e) => setWeb(e.target.value)}
              placeholder="Página web"
              prefix={<LinkOutlined style={{ color: " #647DEE" }} />}
              style={{ marginBottom: 30, marginTop: 20 }}
            />
            <Button
              onClick={() => updateAccount()}
              type="primary"
              shape="round"
              style={{ height: 50, width: 250 }}
            >
              Guardar cambios
            </Button>
          </div>
        </div>
      </div>

      <div className="cont">
        <h2>Dirección</h2>
        <div className="cont_account_set">
          <p>Calle</p>
          <Input
            size="large"
            type="email"
            defaultValue={calle}
            onChange={(e) => setCalle(e.target.value)}
            placeholder="Calle"
            prefix={<EnvironmentOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />

          <p>Código postal</p>
          <Input
            size="large"
            type="email"
            defaultValue={cp}
            onChange={(e) => setCp(e.target.value)}
            placeholder="Código postal"
            prefix={<EnvironmentOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />

          <p>Número</p>
          <Input
            size="large"
            type="email"
            defaultValue={numero}
            onChange={(e) => setNumero(e.target.value)}
            placeholder="Número"
            prefix={<EnvironmentOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />

          <p>Ciudad</p>
          <Input
            size="large"
            type="email"
            defaultValue={city}
            onChange={(e) => setCity(e.target.value)}
            placeholder="Ciudad"
            prefix={<EnvironmentOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />

          <p>Provincia</p>
          <Input
            size="large"
            type="email"
            defaultValue={provincia}
            onChange={(e) => setProvincia(e.target.value)}
            placeholder="Provincia"
            prefix={<EnvironmentOutlined style={{ color: " #647DEE" }} />}
            style={{ marginBottom: 30 }}
          />
          <Button
            onClick={() => updateAccount()}
            type="primary"
            shape="round"
            style={{ height: 50, width: 250 }}
          >
            Guardar cambios
          </Button>
        </div>
      </div>

      <div className="cont">
        <h2>Acciones de la cuenta</h2>
        <div className="cont_account_set">
          <p>Cambiar contraseña</p>
          <Button
            onClick={() => resetPassword()}
            type="dashed"
            shape="round"
            style={{ height: 50, width: 250, marginTop: 30 }}
          >
            Cambiar contraseña
          </Button>
        </div>
      </div>

      <div className="cont">
        <h2>Zona peligrosa</h2>
        <div className="cont_account_set">
          <p>Eliminar cuenta</p>
          <Button
            onClick={() => deleteAccount()}
            type="primary"
            shape="round"
            danger
            style={{ height: 50, width: 250, marginTop: 30 }}
          >
            Eliminar cuenta
          </Button>
        </div>
      </div>
    </div>
  );
}

export default withRouter(Account);
