import React, { useState } from "react";
import "./card.scss";
import {
  Popconfirm,
  message,
  Modal,
  Input,
  Upload,
  Avatar,
  Button,
  DatePicker,
  Switch,
} from "antd";
import { EditOutlined, DeleteOutlined, UserOutlined } from "@ant-design/icons";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import {
  UPLOAD_FILE,
  ACTUALIZAR_MASCOTA,
  ELIMINAR_MASCOTA,
} from "../../../GraphQL/mutation";
import { useMutation } from "react-apollo";
import moment from "moment";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function CardMascotas(props) {
  const [visible, setVisible] = useState(false);
  const [datas, setDatas] = useState(null);
  const { mascota, key, refetch } = props;

  const [avatar, setAvatar] = useState(mascota.avatar);
  const [age, setAge] = useState(mascota.age);
  const [castrado, setCastrado] = useState(mascota.castrado);
  const [name, setname] = useState(mascota.name);
  const [especie, setespecie] = useState(mascota.especie);
  const [raza, setraza] = useState(mascota.raza);
  const [genero, setgenero] = useState(mascota.genero);
  const [peso, setpeso] = useState(mascota.peso);
  const [vacunas, setvacunas] = useState(mascota.vacunas);
  const [alergias, setalergias] = useState(mascota.alergias);
  const [ciudad, setciudad] = useState(mascota.ciudad);
  const [descripcion, setdescripcion] = useState(mascota.descripcion);

  const onSwitchChange = () => {
    if (castrado === true) {
      setCastrado(false);
    } else {
      setCastrado(true);
    }
  };
  const [singleUpload] = useMutation(UPLOAD_FILE);
  const [actualizarMascota] = useMutation(ACTUALIZAR_MASCOTA);
  const [eliminarMascota] = useMutation(ELIMINAR_MASCOTA);

  function confirm(id) {
    eliminarMascota({ variables: { id: id } }).then((res) => {
      if (res.data.eliminarMascota.success) {
        message.success("Mascota eliminada con éxito");
        refetch();
      }
    });
  }

  function onChange(date, dateString) {
    setAge(dateString);
  }

  const uplodaAvatar = (imgBlob) => {
    singleUpload({ variables: { imgBlob } })
      .then((res) => {
        const filename =
          res && res.data && res.data.singleUpload
            ? res.data.singleUpload.filename
            : "";
        refetch();
        setAvatar(filename);
      })
      .catch((error) => {
        console.log("fs error: ", error);
      });
  };

  const anadirMascota = () => {
    const input = {
      id: mascota.id,
      name: name,
      age: age,
      avatar: avatar,
      especie: especie,
      raza: raza,
      genero: genero,
      peso: peso,
      vacunas: vacunas,
      alergias: alergias,
      protectoraID: mascota.protectoraID,
      ciudad: ciudad,
      descripcion: descripcion,
      protectora: true,
      castrado: castrado,
    };
    actualizarMascota({ variables: { input: input } })
      .then((res) => {
        if (res.data.actualizarMascota.success) {
          message.success("Datos actualizado con éxito");
          refetch();
          setVisible(false);
        } else {
          message.error("Algo va mal intentalo de nuevo");
        }
      })
      .catch((e) => {
        message.error("Algo va mal intentalo de nuevo");
        console.log(e);
      });
  };

  return (
    <>
      <div className="card_mascota" key={key}>
        <img src={IMAGES_PATH + mascota.avatar} alt={mascota.name} />
        <div className="card_mascota__info">
          <div>
            <h2>{mascota.name}</h2>
            <p>{moment(mascota.age).format("ll")}</p>
          </div>
          <div style={{ marginLeft: "auto" }}>
            <EditOutlined
              onClick={() => {
                setVisible(true);
                setDatas(mascota);
              }}
              style={{ fontSize: 24, color: "#647dee", cursor: "pointer" }}
            />
            <Popconfirm
              title="¿Estás segur@ que deseas eliminar esta mascota?"
              onConfirm={() => confirm(mascota.id)}
              onCancel={() => {}}
              okText="Si"
              cancelText="No"
            >
              <DeleteOutlined
                style={{
                  fontSize: 24,
                  color: "#f5365c",
                  marginLeft: 20,
                  cursor: "pointer",
                }}
              />
            </Popconfirm>
          </div>
        </div>
      </div>
      {datas ? (
        <Modal
          title="Editar mascota"
          visible={visible}
          footer={false}
          onCancel={() => {
            setVisible(false);
            setDatas(null);
          }}
        >
          <div className="macota__container__items_childern_form">
            <p>Editar</p>

            <div style={{ marginBottom: 20, marginTop: 20 }}>
              {!mascota.avatar ? (
                <Avatar size={100} icon={<UserOutlined />} />
              ) : (
                <Avatar size={100} src={IMAGES_PATH + mascota.avatar} />
              )}

              <Upload
                name="avatar"
                listType="picture"
                showUploadList={false}
                customRequest={async (data) => {
                  let imgBlob = await getBase64(data.file);
                  uplodaAvatar(imgBlob);
                }}
              >
                <Button
                  type="primary"
                  shape="round"
                  style={{ marginTop: 20, marginLeft: 20 }}
                >
                  Seleccionar imagen
                </Button>
              </Upload>
            </div>

            <Input
              size="large"
              type="text"
              defaultValue={mascota.name}
              onChange={(e) => setname(e.target.value)}
              placeholder="Nombre de la mascota"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              defaultValue={mascota.especie}
              onChange={(e) => setespecie(e.target.value)}
              placeholder="Especie"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              defaultValue={mascota.raza}
              onChange={(e) => setraza(e.target.value)}
              placeholder="Raza"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              defaultValue={mascota.genero}
              onChange={(e) => setgenero(e.target.value)}
              placeholder="Género"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="number"
              maxLength={2}
              defaultValue={mascota.peso}
              onChange={(e) => setpeso(e.target.value)}
              placeholder="Peso"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              defaultValue={mascota.vacunas}
              onChange={(e) => setvacunas(e.target.value)}
              placeholder="Vacunas"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              defaultValue={mascota.alergias}
              onChange={(e) => setalergias(e.target.value)}
              placeholder="Alergias"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              defaultValue={mascota.ciudad}
              onChange={(e) => setciudad(e.target.value)}
              placeholder="Ciudad"
              style={{ marginBottom: 30 }}
            />

            <DatePicker
              onChange={onChange}
              placeholder="Fecha de nacimiento"
              style={{ marginBottom: 30 }}
            />

            <div style={{ display: "flex" }}>
              <Switch
                defaultChecked={mascota.castrado}
                onChange={() => onSwitchChange()}
              />
              <p style={{ marginLeft: "auto" }}>¿Está castrado /a?</p>
            </div>

            <Input
              size="large"
              type="text"
              defaultValue={mascota.descripcion}
              onChange={(e) => setdescripcion(e.target.value)}
              placeholder="Descrición"
              style={{ marginBottom: 30 }}
            />

            <Button
              onClick={() => anadirMascota()}
              type="primary"
              shape="round"
              style={{ height: 50, width: 250 }}
            >
              Actualizar Mascota
            </Button>
          </div>
        </Modal>
      ) : null}
    </>
  );
}
