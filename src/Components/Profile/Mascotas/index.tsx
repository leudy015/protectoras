import React, { useState } from "react";
import {
  Input,
  Button,
  DatePicker,
  Upload,
  Avatar,
  Switch,
  Spin,
  message,
} from "antd";
import { UserOutlined } from "@ant-design/icons";
import "./mascota.scss";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import { UPLOAD_FILE, NUEVA_MASCOTA } from "../../../GraphQL/mutation";
import { GET_MASCOTAS } from "../../../GraphQL/query";
import { useMutation, useQuery } from "react-apollo";
import CardMascota from "./cardMascotas";
import Nodata from "../../../Assets/images/nodata.png";

function getBase64(file: any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

export default function Mascota(props: any) {
  const { user } = props;
  const [avatar, setAvatar] = useState("");
  const [age, setAge] = useState("");
  const [castrado, setCastrado] = useState(false);
  const [name, setname] = useState("");
  const [especie, setespecie] = useState("");
  const [raza, setraza] = useState("");
  const [genero, setgenero] = useState("");
  const [peso, setpeso] = useState("");
  const [vacunas, setvacunas] = useState("");
  const [alergias, setalergias] = useState("");
  const [ciudad, setciudad] = useState("");
  const [descripcion, setdescripcion] = useState("");

  const onSwitchChange = () => {
    if (castrado === true) {
      setCastrado(false);
    } else {
      setCastrado(true);
    }
  };
  const [singleUpload] = useMutation(UPLOAD_FILE);
  const [crearMascota] = useMutation(NUEVA_MASCOTA);

  const { data, loading, refetch } = useQuery(GET_MASCOTAS, {
    variables: { id: user.id },
  });

  refetch();

  const mascota =
    data && data.getMascotaprotectora ? data.getMascotaprotectora.list : [];

  function onChange(date: any, dateString: any) {
    setAge(dateString);
  }

  const uplodaAvatar = (imgBlob: any) => {
    singleUpload({ variables: { imgBlob } })
      .then((res: any) => {
        const filename =
          res && res.data && res.data.singleUpload
            ? res.data.singleUpload.filename
            : "";
        setAvatar(filename);
      })
      .catch((error: any) => {
        console.log("fs error: ", error);
      });
  };

  const anadirMascota = () => {
    const input = {
      name: name,
      age: age,
      avatar: avatar,
      especie: especie,
      raza: raza,
      genero: genero,
      peso: peso,
      vacunas: vacunas,
      alergias: alergias,
      protectoraID: user.id,
      ciudad: ciudad,
      descripcion: descripcion,
      protectora: true,
      castrado: castrado,
    };

    crearMascota({ variables: { input: input } })
      .then((res) => {
        if (res.data.crearMascota.success) {
          message.success("Mascota añadida con éxito");
          refetch();
        } else {
          message.error("Algo va mal intentalo de nuevo");
        }
      })
      .catch((e) => {
        message.error("Algo va mal intentalo de nuevo");
        console.log(e);
      });
  };

  return (
    <div className="macota__container">
      <div className="macota__container__items">
        <div className="macota__container__items_childern">
          <h1>Añadir una mascota</h1>
          <div className="macota__container__items_childern_form">
            <p>Añadir</p>

            <div style={{ marginBottom: 20, marginTop: 20 }}>
              {!avatar ? (
                <Avatar size={100} icon={<UserOutlined />} />
              ) : (
                <Avatar size={100} src={IMAGES_PATH + avatar} />
              )}

              <Upload
                name="avatar"
                listType="picture"
                showUploadList={false}
                customRequest={async (data: any) => {
                  let imgBlob = await getBase64(data.file);
                  uplodaAvatar(imgBlob);
                }}
              >
                <Button
                  type="primary"
                  shape="round"
                  style={{ marginTop: 20, marginLeft: 20 }}
                >
                  Seleccionar imagen
                </Button>
              </Upload>
            </div>

            <Input
              size="large"
              type="text"
              onChange={(e) => setname(e.target.value)}
              placeholder="Nombre de la mascota"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              onChange={(e) => setespecie(e.target.value)}
              placeholder="Especie"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              onChange={(e) => setraza(e.target.value)}
              placeholder="Raza"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              onChange={(e) => setgenero(e.target.value)}
              placeholder="Género"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="number"
              maxLength={2}
              onChange={(e) => setpeso(e.target.value)}
              placeholder="Peso"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              onChange={(e) => setvacunas(e.target.value)}
              placeholder="Vacunas"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              onChange={(e) => setalergias(e.target.value)}
              placeholder="Alergias"
              style={{ marginBottom: 30 }}
            />

            <Input
              size="large"
              type="text"
              onChange={(e) => setciudad(e.target.value)}
              placeholder="Ciudad"
              style={{ marginBottom: 30 }}
            />

            <DatePicker
              onChange={onChange}
              placeholder="Fecha de nacimiento"
              style={{ marginBottom: 30 }}
            />

            <div style={{ display: "flex" }}>
              <Switch
                defaultChecked={castrado}
                onChange={() => onSwitchChange()}
              />
              <p style={{ marginLeft: "auto" }}>¿Está castrado /a?</p>
            </div>

            <Input
              size="large"
              type="text"
              onChange={(e) => setdescripcion(e.target.value)}
              placeholder="Descrición"
              style={{ marginBottom: 30 }}
            />

            <Button
              onClick={() => anadirMascota()}
              type="primary"
              shape="round"
              style={{ height: 50, width: 250 }}
            >
              Añadir Mascota
            </Button>
          </div>
        </div>
        <div className="macota__container__items_childern">
          <h1>Mis mascotas</h1>

          {loading ? (
            <div
              style={{
                textAlign: "center",
                justifyContent: "center",
                alignItems: "center",
                display: "flex",
                height: "60vh",
              }}
            >
              <Spin size="large" />
            </div>
          ) : (
            <>
              {mascota.length > 0 ? (
                <div className="macota__container__items_childern__mas">
                  {mascota.map((da: any, i: any) => (
                    <CardMascota mascota={da} key={i} refetch={refetch} />
                  ))}
                </div>
              ) : (
                <div className="no_data">
                  <img src={Nodata} alt="" />
                  <h3>Aún no haz añadido ninguna mascota</h3>
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
}
