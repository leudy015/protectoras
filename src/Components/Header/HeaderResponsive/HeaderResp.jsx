import React from "react";
import { slide as Menu } from "react-burger-menu";
import "./HeadersResp.scss";
import { AlignRightOutlined } from "@ant-design/icons";

class HeaderResp extends React.Component {
  render() {
    return (
      <Menu
        right
        customBurgerIcon={
          <AlignRightOutlined style={{ fontSize: 30, color: "#647DEE" }} />
        }
      >
        {!this.props.data ? (
          <a href="/login" className="btn-custom-ptimary">
            Iniciar sesión
          </a>
        ) : null}
        {!this.props.data ? (
          <a href="/register" className="btn-custom-ptimary">
            Registrarme Ahora!
          </a>
        ) : null}
        <a
          id="about"
          className="menu-item"
          href="https://blog.vetec.es"
          target="blak"
          style={{ marginTop: 30 }}
        >
          Blog
        </a>
        <a id="home" className="menu-item" href="https://vetec.es">
          Vetec
        </a>
      </Menu>
    );
  }
}

export default HeaderResp;
