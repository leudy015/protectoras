import React from "react";
import "../Header.scss";
import { Link, withRouter } from "react-router-dom";
import Logo from "../../../Assets/images/logo.svg";
import { Avatar } from "antd";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import HeaderRes from "../HeaderResponsive/HeaderResp";

function LoginHeader(props: any) {
  const { data } = props;
  const user = data && data.getProtectoras ? data.getProtectoras.data : {};
  return (
    <div className="header_container">
      <div className="header_container_items">
        <div className="header_container_items_left">
          <Link to="/">
            <img src={Logo} alt="isLink" />
          </Link>
          <ul>
            <li>
              <a href="https://blog.vetec.es">Blog</a>
            </li>
            <li>
              <a href="https://vetec.es">Vetec</a>
            </li>
          </ul>
        </div>

        <div className="header_container_items_rigth_loged">
          <Link to="/admin">
            <Avatar src={`${IMAGES_PATH}${user.logo}`} />
          </Link>
          <Link to="/admin">
            <p>{user.name}</p>
          </Link>

          <div className="response_menu">
            <HeaderRes data={data} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(LoginHeader);
