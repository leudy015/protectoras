import React from "react";
import "../Header.scss";
import { Link } from "react-router-dom";
import Logo from "../../../Assets/images/logo.svg";
import $ from "jquery";
import HeaderRes from "../HeaderResponsive/HeaderResp";

$(function () {
  $(window).scroll(function () {
    //@ts-ignore
    if ($(this).scrollTop() > 30) {
      $(".header_container").addClass("header_container_shadow");
    } else {
      $(".header_container").removeClass("header_container_shadow");
    }
  });
});

export default function OauthHeader() {
  return (
    <div className="header_container">
      <div className="header_container_items">
        <div className="header_container_items_left">
          <Link to="/">
            <img src={Logo} alt="isLink" />
          </Link>
          <ul>
            <li>
              <a href="https://blog.vetec.es">Blog</a>
            </li>
            <li>
              <a href="https://vetec.es">Vetec</a>
            </li>
          </ul>
        </div>

        <div className="header_container_items_rigth">
          <Link to="/login">Iniciar sesion</Link>
          <Link to="/register">Registrarme Ahora!</Link>
        </div>

        <div className="response_menu">
          <HeaderRes />
        </div>
      </div>
    </div>
  );
}
