import gql from "graphql-tag";

export const AUTENTICAR_USUARIO = gql`
  mutation autenticarProtectora($email: String!, $password: String!) {
    autenticarProtectora(email: $email, password: $password) {
      success
      message
      data {
        token
        id
      }
    }
  }
`;

export const NUEVO_USUARIO = gql`
  mutation crearProtectora($input: ProtectoraInput) {
    crearProtectora(input: $input) {
      success
      message
    }
  }
`;

export const ACTUALIZAR_USUARIO = gql`
  mutation actualizarProtectora($input: ProtectoraInputActualizar) {
    actualizarProtectora(input: $input) {
      success
      message
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const ELIMINAR_USUARIO = gql`
  mutation eliminarProtectora($id: ID!) {
    eliminarProtectora(id: $id) {
      success
      message
    }
  }
`;

export const ACTUALIZAR_SOLICITUD = gql`
  mutation actualizarsolicitud($input: AdoptarActualizarInput) {
    actualizarsolicitud(input: $input) {
      success
      message
    }
  }
`;

export const ELIMINAR_SOLICITUD = gql`
  mutation eliminarSolicitud($id: ID) {
    eliminarSolicitud(id: $id) {
      success
      message
    }
  }
`;

export const NUEVA_MASCOTA = gql`
  mutation crearMascota($input: Mascotainput) {
    crearMascota(input: $input) {
      success
      message
      data {
        name
        age
        avatar
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
        protectoraID
        ciudad
        protectora
      }
    }
  }
`;

export const ACTUALIZAR_MASCOTA = gql`
  mutation actualizarMascota($input: ActualizarMascotainput) {
    actualizarMascota(input: $input) {
      success
      message
    }
  }
`;

export const ELIMINAR_MASCOTA = gql`
  mutation eliminarMascota($id: ID!) {
    eliminarMascota(id: $id) {
      success
      message
    }
  }
`;
