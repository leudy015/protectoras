import gql from "graphql-tag";

export const USUARIO_ACTUAL = gql`
  query getProtectoras($id: ID) {
    getProtectoras(id: $id) {
      id
      name
      email
      logo
      adrees {
        calle
        cp
        numero
        ciudad
        provincia
        lat
        lgn
      }
      phone
      password
      descripcion
      active
      web
    }
  }
`;

export const GET_SOLICITUD = gql`
  query getAdopcionProtectora($protectora: ID, $estado: String) {
    getAdopcionProtectora(protectora: $protectora, estado: $estado) {
      success
      message
      data {
        id
        name
        lastName
        email
        city
        phone
        UserID
        created_at
        experiencia
        otra
        fecha
        nino
        mascota
        macotas {
          id
          name
          age
          avatar
          usuario
          especie
          raza
          genero
          peso
          microchip
          vacunas
          alergias
          castrado
          protectoraID
          protectora
          ciudad
          descripcion
        }
        usuario
        protectora
        estado
        razon
      }
    }
  }
`;

export const GET_MASCOTAS = gql`
  query getMascotaprotectora($id: ID) {
    getMascotaprotectora(id: $id) {
      message
      success
      list {
        id
        name
        age
        avatar
        usuario
        especie
        raza
        genero
        peso
        microchip
        vacunas
        alergias
        castrado
        protectoraID
        protectora
        ciudad
        descripcion
      }
    }
  }
`;
