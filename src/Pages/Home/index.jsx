import React from "react";
import "./home.scss";
import { Headers } from "./Header";
import Footer from "../../Components/Footer";
import Imagen2 from "../../Assets/images/mai.jpg";
import Imagen1 from "../../Assets/images/closeup-shot-adorable-golden-retriever-puppy-looking-up.jpg";
import Imagen3 from "../../Assets/images/portrait-woman-with-her-beautiful-dog.jpg";
import CookieConsent from "react-cookie-consent";

var TxtType = function (el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = "";
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function () {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

  var that = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) {
    delta /= 2;
  }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function () {
    that.tick();
  }, delta);
};

window.onload = function () {
  var elements = document.getElementsByClassName("typewrite");
  for (var i = 0; i < elements.length; i++) {
    var toRotate = elements[i].getAttribute("data-type");
    var period = elements[i].getAttribute("data-period");
    if (toRotate) {
      new TxtType(elements[i], JSON.parse(toRotate), period);
    }
  }
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #eee}";
  document.body.appendChild(css);
};

export default function Home() {
  return (
    <div className="barner_home">
      <Headers />
      <div className="hero_home">
        <h1
          className="typewrite"
          data-period="2000"
          data-type='[ "Hay un animal de compañía para cada persona.", "Encuentra mascotas a tus fines", "Ayudanos alograr el objetivo"]'
        >
          <span className="wrap"></span>
        </h1>
        <p>Hay un animal de compañía para cada persona.</p>
        <a href="/register" className="btn-grad">
          Regístra tu protectora!
        </a>
      </div>

      <div className="wave"></div>
      <div className="wave2"></div>
      <div className="wave3"></div>
      <div className="wave4"></div>

      <div className="contenido_Home">
        <div className="contenido_Home__item__home__container">
          <div className="contenido_Home__item__home__container__childrem">
            <div>
              <img src={Imagen1} alt="" />
            </div>
            <div>
              <h1>Hay un animal de compañía para cada persona.</h1>
              <p>
                Adopta una mascota y ayudala a tener un hogar y una familia.
                Cada vez son más las personas que deciden compartir su vida con
                un animal. tu que espera para ayudarnos a lograr el objetivo.!
              </p>
            </div>
          </div>
        </div>

        <div className="contenido_Home__item__home__container">
          <div className="contenido_Home__item__home__container__childrem">
            <div className="paramovil">
              <img src={Imagen2} alt="" />
            </div>
            <div>
              <h1>Porqué adoptar y no comprar animales.</h1>
              <p>
                Si estás pensando en adquirir una mascota deberías saber que hay
                muchos perros y gatos en protectoras, perreras municipales e
                incluso en manos de particulares que ya nos los quieren,
                esperando ser adoptados. Cuándo compras un animal en una tienda,
                criadero profesional o directamente de un particular, estás
                fomentando un negocio con vidas, las cuales en la gran mayoría
                de las ocasiones no son tratados con respeto si no como mera
                mercancía de trabajo y de hacer dinero.
              </p>
            </div>
            <div className="dektop">
              <img src={Imagen2} alt="" />
            </div>
          </div>
        </div>

        <div className="contenido_Home__item__home__container">
          <div className="contenido_Home__item__home__container__childrem">
            <div>
              <img src={Imagen3} alt="" />
            </div>
            <div>
              <h1>¿Cómo me cambió la vida mi perro?.</h1>
              <p>
                Tener un perro siempre te cambia la vida, pero si te planteas
                las preguntas adecuadas antes de elegir tu nueva mascota, podrás
                decidir mejor si se adapta más a ti un perro adulto o un
                cachorro.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          textAlign: "center",
          marginBottom: 50,
          width: "100%",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <a href="/register" className="btn-grad2">
          Resgistra tu protectora!
        </a>
      </div>
      <CookieConsent
        location="bottom"
        buttonText="Aceptar"
        cookieName="myAwesomeCookieName2"
        style={{
          background: "#eeeeee",
          color: "black",
        }}
        buttonStyle={{
          color: "white",
          fontSize: "14px",
          backgroundColor: "#647DEE",
          borderRadius: 5,
          width: 150,
          outline: "none",
        }}
        expires={150}
      >
        Este sitio web utiliza cookies para garantizar que obtenga la mejor
        experiencia en nuestro sitio web.{" "}
        <span style={{ fontSize: "10px" }}>
          Más detalles in https://vetec.es/cookies
        </span>
      </CookieConsent>

      <Footer />
    </div>
  );
}
