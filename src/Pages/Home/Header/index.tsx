import React from "react";
import AouthHeader from "./oauthHeader";
import LoginHeader from "./LoginHeader";
import { useQuery } from "react-apollo";
import { USUARIO_ACTUAL } from "../../../GraphQL/query";

export const Headers = () => {
  const id = localStorage.getItem("id");
  const { data, refetch } = useQuery(USUARIO_ACTUAL, { variables: { id } });
  refetch();
  const aouth = data && data.getProtectoras ? data.getProtectoras : false;
  let barra = aouth ? <LoginHeader data={data} /> : <AouthHeader />;

  return <div>{barra}</div>;
};
