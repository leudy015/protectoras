import React from "react";
import "../Header.scss";
import { Link, withRouter } from "react-router-dom";
import { Avatar } from "antd";
import { IMAGES_PATH } from "../../../../Utils/UrlConfig";
import HeaderRes from "../HeaderResponsive/HeaderResp";

function LoginHeader(props: any) {
  const { data } = props;
  const user = data && data.getProtectoras ? data.getProtectoras : {};
  console.log(user);
  return (
    <div className="header_container_home">
      <div className="header_container_items">
        <div className="header_container_items_left">
          <Link to="/">
            <div className="logo" />
          </Link>
          <ul>
            <li>
              <a href="https://blog.vetec.es" className="is">
                Blog
              </a>
            </li>
            <li>
              <a href="https://vetec.es" className="is">
                Vetec
              </a>
            </li>
          </ul>
        </div>

        <div className="header_container_items_rigth_loged">
          <Link to="/admin">
            <Avatar src={`${IMAGES_PATH}${user.logo}`} />
          </Link>
          <Link to="/admin" className="is">
            <p>{user.name}</p>
          </Link>

          <div className="response_menu">
            <HeaderRes data={data} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(LoginHeader);
