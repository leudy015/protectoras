import React from "react";
import { slide as Menu } from "react-burger-menu";
import "./HeadersResp.scss";
import { AlignRightOutlined } from "@ant-design/icons";
import $ from "jquery";

$(function () {
  $(window).scroll(function () {
    //@ts-ignore
    if ($(this).scrollTop() > 30) {
      $(".me").addClass("me_w");
    } else {
      $(".me").removeClass("me_w");
    }
  });
});

class HeaderResp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Menu right customBurgerIcon={<AlignRightOutlined className="me" />}>
        {!this.props.data ? (
          <a href="/login" className="btn-custom-ptimary">
            Log In
          </a>
        ) : null}
        {!this.props.data ? (
          <a href="/register" className="btn-custom-ptimary">
            Register now free!
          </a>
        ) : null}
        {this.props.data ? (
          <a href="/admin" className="btn-custom-ptimary">
            My admin
          </a>
        ) : null}

        <a
          id="about"
          className="menu-item"
          href="https://blog.vetec.es"
          target="blak"
          style={{ marginTop: 30 }}
        >
          Blog
        </a>
        <a id="home" className="menu-item" href="https://vetec.es">
          Vetec
        </a>
      </Menu>
    );
  }
}

export default HeaderResp;
