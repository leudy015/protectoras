import React from "react";
import "../Header.scss";
import { Link } from "react-router-dom";
import $ from "jquery";
import HeaderRes from "../HeaderResponsive/HeaderResp";

$(function () {
  $(window).scroll(function () {
    //@ts-ignore
    if ($(this).scrollTop() > 30) {
      $(".header_container_home").addClass("header_container_shadow_home");
      $(".is").addClass("is_green");
      $(".logo").addClass("logo_black");
      $(".bt").addClass("bt_active");
    } else {
      $(".header_container_home").removeClass("header_container_shadow_home");
      $(".is").removeClass("is_green");
      $(".logo").removeClass("logo_black");
      $(".bt").removeClass("bt_active");
    }
  });
});

export default function OauthHeader() {
  return (
    <div className="header_container_home">
      <div className="header_container_items">
        <div className="header_container_items_left">
          <Link to="/">
            <div className="logo" />
          </Link>
          <ul>
            <li>
              <a href="https://blog.vetec.es" className="is">
                Blog
              </a>
            </li>
            <li>
              <a href="https://vetec.es" className="is">
                Vetec
              </a>
            </li>
          </ul>
        </div>

        <div className="header_container_items_rigth">
          <Link to="/login" className="bt">
            Iniciar sesión
          </Link>
          <Link to="/register" className="bt">
            Resgístrarme Ahora!
          </Link>
        </div>

        <div className="response_menu">
          <HeaderRes />
        </div>
      </div>
    </div>
  );
}
