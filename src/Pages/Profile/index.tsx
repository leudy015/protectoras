import React, { useEffect } from "react";
import ProfileContent from "../../Container/Profile";

export default function Profile() {
  useEffect(() => {
    document.title = "Consola de administración";
  }, []);
  return <ProfileContent />;
}
