import React from "react";
import { Result, Button } from "antd";
import Barner from "../../Components/Barner";
import Footer from "../../Components/Footer";

export default function index() {
  return (
    <div>
      <Barner title="Sorry, the page you visited does not exist." />
      <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Button type="primary" shape="round" href="/">
            Back Home
          </Button>
        }
      />
      <Footer />
    </div>
  );
}
