import React, { useEffect } from "react";
import LoginForm from "../../Container/Login/LoginForm/Loginform";
import "../../Container/Login/LoginForm/Login.scss";
import { Headers } from "../../Components/Header";
import Footer from "../../Components/Footer";

export default function Login() {
  useEffect(() => {
    document.title = "Iniciar sesión";
  }, []);

  const img = require("../../Assets/images/Select-pana.svg");

  return (
    <>
      <Headers />
      <div className="containers">
        <div className="containers-login">
          <div className="Containerform-login">
            <div className="img_cont">
              <img src={img} alt="Register" className="landingRe" />
            </div>
            <div className="form_cont_login">
              <LoginForm />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
