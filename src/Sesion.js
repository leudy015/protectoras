import React from "react";
import { Query } from "react-apollo";
import { USUARIO_ACTUAL } from "./GraphQL/query";

//@ts-ignore
const Session = (Component) => (props) => {
  const id = localStorage.getItem("id");
  return (
    <Query query={USUARIO_ACTUAL} variables={{ id: id }}>
      {({ data, refetch }) => {
        return <Component {...props} refetch={refetch} session={data} />;
      }}
    </Query>
  );
};

export default Session;
