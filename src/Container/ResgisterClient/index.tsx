import React, { useEffect } from "react";
import "./Register.scss";
import RegistrationForm from "./Registerform";
import { Headers } from "../../Components/Header";
import Footer from "../../Components/Footer";

const img = require("../../Assets/images/landingregister.svg");

function RegisterClient() {
  useEffect(() => {
    document.title = "Registro";
  }, []);

  return (
    <>
      <Headers />
      <div className="containers">
        <div className="ContainerformRegister">
          <div className="img_cont">
            <img src={img} alt="Register" className="landingRe" />
          </div>
          <div className="form_cont">
            <RegistrationForm />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}

export default RegisterClient;
