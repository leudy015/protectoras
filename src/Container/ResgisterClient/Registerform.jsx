import React, { useState, useEffect } from "react";
import { Form, Input, Checkbox, Button, message } from "antd";
import { NUEVO_USUARIO } from "../../GraphQL/mutation";
import { Mutation } from "react-apollo";
import { useHistory } from "react-router-dom";
import LoginForm from "../Login/LoginForm/Loginform";
import ReCAPTCHA from "react-google-recaptcha";

const TEST_SITE_KEY = "6LfNG9IZAAAAAOB3uD2zhJUxbSXd9t_YvwXRmvUu";
const DELAY = 1500;

const RegistrationForm = () => {
  const [values, setValues] = useState("");
  const [load, setLoad] = useState(false);
  const [expired, setExpired] = useState(false);

  const _reCaptchaRef = React.createRef();

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoad(true);
    }, DELAY);
    return () => clearTimeout(timer);
  }, []);

  const handleChange = (value) => {
    setValues(value);
    // if value is null recaptcha expired
    if (values === null) setExpired(true);
  };

  const history = useHistory();
  const [form] = Form.useForm();
  return (
    <div className="register">
      <p className="psol">Bienvenid@ a Vetec para Protectoras</p>
      <Mutation mutation={NUEVO_USUARIO}>
        {(crearProtectora) => {
          return (
            <Form
              form={form}
              name="register"
              onFinish={(values) => {
                const handleSubmit = async () => {
                  await crearProtectora({
                    variables: {
                      input: {
                        name: values.name,
                        email: values.email,
                        password: values.password,
                        phone: values.phone,
                      },
                    },
                    //@ts-ignore
                  }).then(async ({ data: res }) => {
                    if (
                      res &&
                      res.crearProtectora &&
                      res.crearProtectora.success
                    ) {
                      message.success(res.crearProtectora.message);
                      const datss =
                        res &&
                        res.crearProtectora &&
                        res.crearProtectora.message;
                      if (datss) {
                        history.push(`/login`);
                      } else {
                        return null;
                      }
                    } else if (
                      res &&
                      res.crearProtectora &&
                      !res.crearProtectora.success
                    )
                      message.error(res.crearProtectora.messages);
                  });
                };
                handleSubmit();
              }}
              scrollToFirstError
            >
              <Form.Item
                name="name"
                rules={[
                  {
                    required: true,
                    message: "Por favor introduce un nombre",
                  },
                ]}
              >
                <Input
                  placeholder="Nombre de la protectora"
                  className="form-control"
                />
              </Form.Item>

              <Form.Item
                name="phone"
                rules={[
                  {
                    type: "string",
                    message: "Por favor introduce un número móvil!",
                  },
                  {
                    required: true,
                    message: "Por favor introduce un número móvil!",
                  },
                ]}
              >
                <Input
                  placeholder="Número móvil"
                  className="form-control"
                  type="number"
                />
              </Form.Item>

              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "Email no valido!",
                  },
                  {
                    required: true,
                    message: "Por favor introduce un email",
                  },
                ]}
              >
                <Input
                  placeholder="Correo electrónico"
                  className="form-control"
                />
              </Form.Item>

              <div className="name_container">
                <div className="int">
                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Por favor introduce una contraseña!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      placeholder="Contraseña"
                      className="form-control"
                    />
                  </Form.Item>
                </div>
                <div className="intp">
                  <Form.Item
                    name="confirm"
                    dependencies={["password"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "por favor confirma tu contraseña!",
                      },
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }

                          return Promise.reject("Las contraseña no coinciden!");
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      placeholder="Confirmar contraseña"
                      className="form-control"
                    />
                  </Form.Item>
                </div>
              </div>
              <Form.Item
                name="agreement"
                valuePropName="checked"
                rules={[
                  {
                    validator: (_, value) =>
                      value
                        ? Promise.resolve()
                        : Promise.reject(
                            "Por favor acepta los términos y condiciones"
                          ),
                  },
                ]}
              >
                <Checkbox>
                  Acepto los términos y condiciones de vetec{" "}
                  <a style={{ color: "#647DEE" }} href="/terms-conditions">
                    Términos y Condiciones
                  </a>
                </Checkbox>
              </Form.Item>
              <Form.Item>
                {load && (
                  <ReCAPTCHA
                    style={{ display: "inline-block" }}
                    ref={_reCaptchaRef}
                    sitekey={TEST_SITE_KEY}
                    onChange={handleChange}
                  />
                )}
                <Button
                  style={{ width: "100%", height: 50 }}
                  shape="round"
                  type="primary"
                  className="btn-btn-primary"
                  htmlType="submit"
                  disabled={values === "" || expired}
                >
                  ¡Registrarme!
                </Button>
                <Form.Item>
                  ¿Aún no tienes una cuenta?{" "}
                  <a style={{ color: "#647DEE" }} href="/login">
                    Iniciar sesión!
                  </a>
                </Form.Item>
              </Form.Item>
              <LoginForm fromSignup={true} />
            </Form>
          );
        }}
      </Mutation>
    </div>
  );
};

export default RegistrationForm;
