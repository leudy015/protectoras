import React from "react";
import "./loginsocia.scss";

export interface ISocialLoginBtn {
  type: "facebook" | "google";
}

export default function LoginsocialBotton(props: ISocialLoginBtn) {
  const { type } = props;

  const facebook = require("../../Assets/images/Facebook.svg");
  const google = require("../../Assets/images/google-icon.svg");

  return (
    <button className="btn__social">
      {type === "facebook" ? <img src={facebook} /> : <img src={google} />}

      {type === "facebook" ? "Continue with Facebook" : "Continue with Google"}
    </button>
  );
}
