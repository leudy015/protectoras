import React, { Component } from "react";
import "./socialLogin.scss";
import { Redirect, withRouter } from "react-router-dom";
//@ts-ignore
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { GoogleLogin } from "react-google-login";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  LOCAL_API_URL,
} from "../../../Utils/UrlConfig";
import LoginsocialBotton from "../../LoginSocialBotton";

class ScocialLogin extends Component {
  state = {
    isAuthenticated: false,
    user: null,
    token: "",
  };

  onFailure = (error: any) => {
    console.log(error);
  };

  facebookResponse = (response: any) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    //@ts-ignore
    fetch(`${LOCAL_API_URL}/api/v1/auth/facebook`, options).then((r) => {
      r.json().then(async (user: any) => {
        if (user.token) {
          //@ts-ignore
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  googleResponse = (response: any) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    //@ts-ignore
    fetch(`${LOCAL_API_URL}/api/v1/auth/google`, options).then((r) => {
      console.log(r);
      r.json().then((user: any) => {
        if (user.token) {
          //@ts-ignore
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  render() {
    if (this.state.isAuthenticated) {
      return <Redirect to="/admin" />;
    } else
      return (
        <div className="socialLogin">
          <FacebookLogin
            appId={FACEBOOK_APP_ID}
            autoLoad={false}
            fields="name,email,picture"
            callback={this.facebookResponse}
            render={(renderProps: any) => (
              <div onClick={renderProps.onClick} style={{ marginRight: 10 }}>
                <LoginsocialBotton type="facebook" />
              </div>
            )}
          />
          <GoogleLogin
            clientId={GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={this.googleResponse}
            onFailure={this.onFailure}
            render={(renderProps: any) => (
              <div
                onClick={renderProps.onClick}
                //@ts-ignore
                disabled={renderProps.disabled}
              >
                <LoginsocialBotton type="google" />
              </div>
            )}
          />
        </div>
      );
  }
}
//@ts-ignore
export default withRouter(ScocialLogin);
