import React, { Component, Fragment } from "react";
import { Input, Checkbox, message, Spin, Button } from "antd";
import "./Login.scss";
import { AUTENTICAR_USUARIO } from "../../../GraphQL/mutation";
import { Mutation } from "react-apollo";
import { withRouter } from "react-router-dom";
//import SocialLogin from "../LoginSocial/socialLogin";
import ReCAPTCHA from "react-google-recaptcha";

const initialState = {
  email: "",
  password: "",
};

const TEST_SITE_KEY = "6Lf9oNIZAAAAANXs9fty4FM2TNsagCl4aXZdwqTu";
const DELAY = 1500;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      callback: "not fired",
      value: "",
      load: false,
      expired: "false",
    };

    this._reCaptchaRef = React.createRef();
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ load: true });
    }, DELAY);
    console.log("didMount - reCaptcha Ref-", this._reCaptchaRef);
  }

  handleChange = (value) => {
    console.log("Captcha value:", value);
    this.setState({ value });
    if (value === null) this.setState({ expired: "true" });
  };

  asyncScriptOnLoad = () => {
    this.setState({ callback: "called!" });
    console.log("scriptLoad - reCaptcha Ref-", this._reCaptchaRef);
  };

  actualizarState = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  limpiarState = () => {
    this.setState({ ...initialState });
  };

  iniciarSesion = (e, usuarioAutenticar) => {
    e.preventDefault();

    usuarioAutenticar().then(async ({ data: res }) => {
      if (res.autenticarProtectora.success) {
        localStorage.setItem("token", res.autenticarProtectora.data.token);
        localStorage.setItem("id", res.autenticarProtectora.data.id);
        this.props.history.push("/admin");
        // Limpiar state
        this.limpiarState();
      } else {
        message.error(res.autenticarProtectora.message);
      }
    });
  };

  /* logon = async (email, password) => {
    this.setState(
      {
        email: email,
        password: password,
        value: "thechatchatfull",
      },
      () => {
        document.getElementById("loginSubmit").click();
      }
    );
  }; */
  validarForm = () => {
    const { email, password, value } = this.state;

    const noValido = !email || !password || !value;
    console.log(noValido);
    return noValido;
  };

  render() {
    console.log(this.props);
    const { email, password } = this.state;

    const { load } = this.state || {};

    return (
      <div className={this.props.fromSignup ? "" : "register-page-content"}>
        <Fragment>
          <Mutation
            mutation={AUTENTICAR_USUARIO}
            variables={{ email, password }}
          >
            {(usuarioAutenticar, { loading }) => {
              if (loading) {
                return (
                  <div className={this.props.fromSignup ? "" : "page-loader"}>
                    <Spin size={this.props.fromSignup ? "" : "large"}></Spin>
                  </div>
                );
              }

              return (
                <div className={this.props.fromSignup ? "" : "containerLogin"}>
                  <div className={this.props.fromSignup ? "" : "contLogin"}>
                    <form
                      onSubmit={(e) => this.iniciarSesion(e, usuarioAutenticar)}
                      className="col-md-8"
                      style={{
                        display: this.props.fromSignup ? "none" : "block",
                      }}
                    >
                      <p className="psol">Inicia sesión</p>

                      <div className="form-group">
                        <Input
                          onChange={this.actualizarState}
                          value={email}
                          type="text"
                          name="email"
                          className="form-control"
                          placeholder="Correo electrónico"
                        />
                      </div>

                      <div className="form-group">
                        <Input.Password
                          onChange={this.actualizarState}
                          value={password}
                          type="password"
                          name="password"
                          className="form-control"
                          placeholder="Contraseña"
                        />
                      </div>

                      {load && (
                        <ReCAPTCHA
                          style={{ display: "inline-block" }}
                          ref={this._reCaptchaRef}
                          sitekey={TEST_SITE_KEY}
                          onChange={this.handleChange}
                          asyncScriptOnLoad={this.asyncScriptOnLoad}
                        />
                      )}

                      <div className="forgot">
                        <Checkbox>Recordarme</Checkbox>
                        <a
                          className="login-form-forgot"
                          href="/oauth/v2/recover-password"
                        >
                          ¿Olvidaste tu contraseña?
                        </a>
                      </div>
                      <div className="form-group">
                        <Button
                          disabled={loading || this.validarForm()}
                          shape="round"
                          type="primary"
                          id="loginSubmit"
                          htmlType="submit"
                          style={{
                            width: "100%",
                            marginTop: 15,
                            marginBottom: 15,
                            height: 50,
                          }}
                        >
                          Iniciar Sesión
                        </Button>
                      </div>

                      <div style={{ marginBottom: 15 }}>
                        ¿Aún no tienes una cuenta?{" "}
                        <a className="forgot" href="/register">
                          Registrarme ahora!
                        </a>
                      </div>
                    </form>
                    {/* <SocialLogin logon={this.logon} fromSignup={true} /> */}
                  </div>
                </div>
              );
            }}
          </Mutation>
        </Fragment>
      </div>
    );
  }
}

export default withRouter(Login);
