import React from "react";
import { withRouter } from "react-router-dom";
import Profiles from "../../Components/Profile/profile";
import { useQuery } from "react-apollo";
import { USUARIO_ACTUAL } from "../../GraphQL/query";

function Profile() {
  const id = localStorage.getItem("id");
  const { data, refetch } = useQuery(USUARIO_ACTUAL, { variables: { id } });
  refetch();
  const user = data && data.getProtectoras ? data.getProtectoras : {};
  return <Profiles data={user} refetch={refetch} />;
}

export default withRouter(Profile);
