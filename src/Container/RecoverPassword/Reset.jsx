import React, { useEffect, useState } from "react";
import "./Forgot.scss";
import ReserForm from "./ResetForm";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { Headers } from "../../Components/Header";
import Footer from "../../Components/Footer";
const ResetClient = (props) => {
  const [resut, setResult] = useState({
    email: "",
    isValid: false,
    set: false,
  });

  const token = props.match.params.token;

  useEffect(() => {
    document.title = "Recuerar Contraseña";
  }, []);

  useEffect(() => {
    // console.log(this.props.match.params.token);
    const url = `${LOCAL_API_URL}/tokenValidation-protectora?token=${token}`;
    axios
      .get(url)
      .then((res) => {
        setResult({
          isValid: res.data.isValid,
          email: res.data.email,
          set: true,
        });
      })
      .catch((err) => {
        console.log("err:", err);
      });
  }, []);

  return (
    <>
      <Headers />
      <div className="containers">
        <div className="Containerformm">
          <p>Introduzca su nueva contraseña!</p>
          {resut.isValid && (
            <ReserForm email={resut.email} token={props.match.params.token} />
          )}
          {!resut.isValid && resut.set && <p>Invalid token, try again.</p>}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default withRouter(ResetClient);
