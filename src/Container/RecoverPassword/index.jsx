import React, { useEffect } from "react";
import "./Forgot.scss";
import NormalforgotForm from "./ForgotForm";
import { Headers } from "../../Components/Header";
import Footer from "../../Components/Footer";

function ForgotClient() {
  useEffect(() => {
    document.title = "Recuperar contraseña";
  }, []);

  return (
    <>
      <Headers />
      <div className="containers">
        <div className="Containerformm">
          <p>Olvidaste tu contraseña!</p>
          <NormalforgotForm />
        </div>
      </div>
      <Footer />
    </>
  );
}

export default ForgotClient;
