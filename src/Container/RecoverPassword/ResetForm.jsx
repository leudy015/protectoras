import React from "react";
import { Form, Input, Button } from "antd";
import "../Login/LoginForm/Login.scss";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { useHistory } from "react-router-dom";
import axios from "axios";

const ResetForm = (props) => {
  const history = useHistory();

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={(values) => {
        const handleSubmit = () => {
          const url = `${LOCAL_API_URL}/resetPassword-protectora?password=${values.password}&email=${props.email}&token=${props.token}"`;
          axios
            .get(url)
            .then((res) => {
              history.push("/login");
              console.log(res.data);
            })
            .catch((err) => {
              console.log("error:", err);
            });
        };
        handleSubmit();
      }}
    >
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: "Por favor introduce una contraseña!",
          },
        ]}
        hasFeedback
      >
        <Input.Password className="form-control" placeholder="Contraseña" />
      </Form.Item>

      <Form.Item
        name="confirm"
        dependencies={["password"]}
        hasFeedback
        rules={[
          {
            required: true,
            message: "Por favor confirma tu contraseña!",
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }

              return Promise.reject("Las contraseñas no coinciden!");
            },
          }),
        ]}
      >
        <Input.Password
          className="form-control"
          placeholder="Confirmar Contraseña"
        />
      </Form.Item>

      <Form.Item>
        <Button
          style={{ width: "100%", height: 50 }}
          shape="round"
          type="primary"
          className="btn-btn-primary"
          htmlType="submit"
        >
          Guardar contraseña
        </Button>
      </Form.Item>
    </Form>
  );
};

export default ResetForm;
