import React, { useState, useEffect } from "react";
import { Form, Input, message, Spin, Button } from "antd";
import "./Forgot.scss";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { useHistory } from "react-router-dom";
import ReCAPTCHA from "react-google-recaptcha";

const TEST_SITE_KEY = "6Lf9oNIZAAAAANXs9fty4FM2TNsagCl4aXZdwqTu";
const DELAY = 1500;

const NormalforgotForm = () => {
  const [loading, setloading] = useState(false);
  const [values, setValues] = useState("");
  const [load, setLoad] = useState(false);
  const [expired, setExpired] = useState(false);
  const history = useHistory();

  const _reCaptchaRef = React.createRef();

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoad(true);
    }, DELAY);
    return () => clearTimeout(timer);
  }, []);

  const handleChange = (value) => {
    console.log("Captcha value:", value);
    setValues(value);
    if (values === null) setExpired(true);
  };

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={(value) => {
        setloading(true);
        const handleSubmit = () => {
          const emails = value.email;
          if (emails) {
            fetch(`${LOCAL_API_URL}/forgotpassword-protectora?email=${emails}`)
              .then((res) => {
                console.log(res);
                if (res) {
                  setloading(false);
                  message.success("Sent message go to your inbox");
                  history.push("/login");
                } else {
                  message.error("there is a problem with your request");
                  setloading(false);
                }
              })
              .catch((err) => {
                console.log("err:", err);
                setloading(false);
              });
          }
        };
        handleSubmit();
        console.log("dome");
      }}
    >
      {loading ? (
        <Spin style={{ marginBottom: 20 }} />
      ) : (
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Por favor introduce un email!" }]}
        >
          <Input placeholder="Correo electrónico" className="form-control" />
        </Form.Item>
      )}

      {load && (
        <ReCAPTCHA
          style={{ display: "inline-block" }}
          ref={_reCaptchaRef}
          sitekey={TEST_SITE_KEY}
          onChange={handleChange}
        />
      )}

      <Form.Item>
        <Button
          style={{ width: "100%", height: 50 }}
          shape="round"
          type="primary"
          className="btn-btn-primary"
          htmlType="submit"
          disabled={values === "" || expired}
        >
          Enviar enlace
        </Button>
      </Form.Item>

      <Form.Item>
        ¿Aún no tienes una cuenta?{" "}
        <a style={{ color: "#647DEE" }} href="/register">
          Regístrarme ahora!
        </a>
      </Form.Item>
    </Form>
  );
};

export default NormalforgotForm;
