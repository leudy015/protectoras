import React from "react";
import { Route, Redirect } from "react-router-dom";

//@ts-ignore
const PublicRoutePro = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      return localStorage.getItem("token") ? (
        <Redirect
          to={{
            pathname: "/admin",
            state: { from: props.location },
          }}
        />
      ) : (
        <Component {...props} refetch={rest.refetch} />
      );
    }}
  />
);

export default PublicRoutePro;
